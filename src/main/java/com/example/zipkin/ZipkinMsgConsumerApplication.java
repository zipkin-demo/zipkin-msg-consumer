package com.example.zipkin;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.JmsListener;

@SpringBootApplication
public class ZipkinMsgConsumerApplication {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public static void main(String[] args) {
		SpringApplication.run(ZipkinMsgConsumerApplication.class, args);
	}

	@JmsListener(destination = "backend")
	public void onMessage() {
		log.info("message received : {} ",new Date().toString());
	}


}
